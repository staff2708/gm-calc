import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


export default () => {
    const firebaseConfig = {
        apiKey: "AIzaSyBs1qjeaFfpVCU2o7elaWwT78NadtGu9Sg",
        authDomain: "gm-bd-tog.firebaseapp.com",
        databaseURL: "https://gm-bd-tog.firebaseio.com",
        projectId: "gm-bd-tog",
        storageBucket: "gm-bd-tog.appspot.com",
        messagingSenderId: "252540194357",
        appId: "1:252540194357:web:327d5e9194cb09bb4deb7c"
    };
    
    firebase.initializeApp(firebaseConfig);
}