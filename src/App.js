
import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './css/App.css';
import Home from './components/home';
import Rahil from './components/rahil';
import Roller from './components/diceRoller';
import firebase from 'firebase/app';
import Calculator from './components/calculator';
import Characters from './components/characters';
import CreateCharacter from './components/createCharacter';
import Fight from './components/fightPage2';

function App() {

  useEffect(() => {
    const login = 'staff2708@gmail.com';
    const password = '123123123';
    try {
      firebase.auth().signInWithEmailAndPassword(login, password);
      console.log('login');
    } catch (e) {
      console.error(e);
    }
  }, [])

  return (
    <div className="App">
        <header className="header">
            <div className="box header__box">
                <div className="nav">
                    <Link to={'/'} className="nav__link"> Главная </Link>
                    <Link to={'/Rahil'} className="nav__link"> Узнать кто Рахиль </Link>
                    <Link to={'/characters'} className="nav__link"> Персонажи</Link>
                    <Link to={'/calculator'} className="nav__link"> Калькулятор</Link>
                    <Link to={'/createCharacter'} className="nav__link">Новый персонаж</Link>
                </div>
            </div>
        </header>
        <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/rahil' component={Rahil} />
            <Route path='/characters' component={Characters}/>
            <Route path='/calculator' component={Fight}/>
            <Route path='/createCharacter' component={CreateCharacter}/>
        </Switch>
        <Roller />
    </div>
  );
}

export default App;
