import React, { useEffect, useState } from 'react';
import firebase from 'firebase/app';
import './fightPage.css';

const Fight = () => {

    useEffect(() => {
        firebase.firestore().collection('characters').get().then((snapshot) => {
            const tempArr = []
            snapshot.docs.forEach(doc => {
                tempArr.push(doc.data());
            })
            setState({...state, characterList: tempArr});
            console.log(tempArr);
        })
    }, [])

    const [state, setState] = useState({
        firstCharacter: {},
        secondCharacter: {},
        characterList: []
    });

    const selectChar = (isFirst, char) => {
        const charPosition = isFirst ? 'firstCharacter': 'secondCharacter';
        setState({...state, [charPosition]: char})
    };

    const getD100 = () => {
        const roll = Math.floor(Math.random() * 100) + 1;
        console.log(roll);
        return roll;
    }

    const calculateMelee = (DodgeChar) => {
        console.log(state);
        const isSuccess = (getD100() - state.firstCharacter.melee * 5) <= (getD100() - state.secondCharacter[DodgeChar] * 5);
        if(isSuccess) {
            const dmg = state.firstCharacter.damage * 5 - state.secondCharacter.shinsu_body_buff * 5;
            if (dmg) {
                alert(`attacked, dmg: ${dmg}`);
            } else {
                alert('attacked, but no damage');
            }
        } else {
            alert('dodged');
        }
    }

    const calculateFencing = (DodgeChar) => {
        console.log(state);
        const isSuccess = (getD100() - state.firstCharacter.fencing * 5) <= (getD100() - state.secondCharacter[DodgeChar] * 5);
        if(isSuccess) {
            const dmg = state.firstCharacter.damage * 5 - state.secondCharacter.shinsu_body_buff * 5;
            if (dmg) {
                alert(`attacked, dmg: ${dmg}`);
            } else {
                alert('attacked, but no damage');
            }
        } else {
            alert('dodged');
        }
    }

    const calculateRange = (DodgeChar) => {
        console.log(state);
        const isSuccess = (getD100() - state.firstCharacter.range * 5) <= (getD100() - state.secondCharacter[DodgeChar] * 5);
        if(isSuccess) {
            const dmg = state.firstCharacter.damage * 5 - state.secondCharacter.shinsu_body_buff * 5;
            if (dmg) {
                alert(`attacked, dmg: ${dmg}`);
            } else {
                alert('attacked, but no damage');
            }
        } else {
            alert('dodged');
        }
    }

    const calculateShinsu = (DodgeChar) => {
        console.log(state);
        const isSuccess = (getD100() - state.firstCharacter.shinsu_control * 5) <= (getD100() - state.secondCharacter[DodgeChar] * 5);
        if(isSuccess) {
            const dmg = state.firstCharacter.soo * 5 - state.secondCharacter.shinsu_resistance * 3;
            if (dmg) {
                alert(`attacked, dmg: ${dmg}`);
            } else {
                alert('attacked, but no damage');
            }
        } else {
            alert('dodged');
        }
    }


    return (
        <div className="fight-page">
            <ul className="selection-list selecton-list__first-character">
                {state.characterList.map(item => {
                    return (
                        <li onClick={() => selectChar(true, item)} className="selection-list__item">{item.name}</li>
                    )
                })}
            </ul>
            <div class="fight-page__control-section">
                <section className="first-player">
                    <h3>Melee</h3>
                    <button onClick={() => calculateMelee('block')}>Melee vs Block</button>
                    <button onClick={() => calculateMelee('dodge')}>Melee vs Dodge</button>
                    <button onClick={() => calculateMelee('fencing')}>Melee vs Fencing</button>
                    <button onClick={() => calculateMelee('shinsu_control')}>Melee vs Shinsu</button>
                    <button onClick={() => calculateMelee('lighthouses')}>Melee vs Lighthouse</button>
                    <h3>Fencing</h3>
                    <button onClick={() => calculateFencing('block')}>Fencing vs Block</button>
                    <button onClick={() => calculateFencing('dodge')}>Fencing vs Dodge</button>
                    <button onClick={() => calculateFencing('fencing')}>Fencing vs Fencing</button>
                    <button onClick={() => calculateFencing('shinsu_control')}>Fencing vs Shinsu</button>
                    <button onClick={() => calculateFencing('lighthouses')}>Fencing vs Lighthouse</button>
                    <h3>Range</h3>
                    <button onClick={() => calculateRange('block')}>Range vs Block</button>
                    <button onClick={() => calculateRange('dodge')}>Range vs Dodge</button>
                    <button onClick={() => calculateRange('fencing')}>Range vs Fencing</button>
                    <button onClick={() => calculateRange('shinsu_control')}>Range vs Shinsu</button>
                    <button onClick={() => calculateRange('lighthouses')}>Range vs Lighthouse</button>
                    <h3>Shinsu</h3>
                    <button onClick={() => calculateShinsu('block')}>Shinsu vs Block</button>
                    <button onClick={() => calculateShinsu('dodge')}>Shinsu vs Dodge</button>
                    <button onClick={() => calculateShinsu('fencing')}>Shinsu vs Fencing</button>
                    <button onClick={() => calculateShinsu('shinsu_control')}>Shinsu vs Shinsu</button>
                    <button onClick={() => calculateShinsu('lighthouses')}>Shinsu vs Lighthouse</button>
                </section>
                <section className="second-player"></section>
            </div>
            <ul className="selection-list selecton-list__second-character">
                {state.characterList.map(item => {
                    return (
                        <li onClick={() => selectChar(false, item)} className="selection-list__item">{item.name}</li>
                    )
                })}
            </ul>
        </div>
    )
}

export default Fight;