import React, { Component } from 'react';

function Home() {
    return (
        <div className="home">
            <div className="box">
                <h1 className="title home__title">Главная</h1>
            </div>
        </div>
    );
}

export default Home;