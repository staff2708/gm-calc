import React, { useEffect, useState, Component } from 'react';
import '../css/roller.css';

function Roller() {
    const [active, setActive] = useState(false);
    const [opposed, setOpposed] = useState(false);
    const [display, setDisplay] = useState(0);
    const [history, setHistory] = useState([]);

    const diceSet = [2, 6, 20, 100];

    const dices = diceSet.map((diceValue, index) =>
        <Dice diceValue = {diceValue}
              opposed = {opposed}
              setDisplay = {setDisplay}
              history = {history}
              setHistory = {setHistory}
              key = {index}
        />
    );

    return (
        <div className={"roller" + (active ? " active" : "")}>
            <button className="roller__trigger" onClick={() => setActive(!active)}>{active ? '>' : '<'}</button>
            <div className="roller__display">{display}</div>
            <div className="roller__diceBox">{dices}</div>
            <button className={"btn roller__btn" + (opposed ? " active" : "")} onClick={() => setOpposed(!opposed)}>Opposed</button>
            <History history={history} />
        </div>
    );
}

function Dice({diceValue, opposed, setDisplay, history, setHistory}) {
    const Roll = (diceValue, opposed, setDisplay, history, setHistory) => {
        let result;
        let historyLog;
        let historyTemp = history;

        const roll = Math.round(1 + Math.random() * (diceValue - 1));

        if(opposed){
            const roll2 = Math.floor(1 + Math.random() * (diceValue - 1));
            result = roll - roll2;
            historyLog = result + " = " + roll + " - " + roll2;
        } else {
            result = roll;
            historyLog = result;
        }

        historyTemp.unshift(historyLog)
        setDisplay(result);
        setHistory(historyTemp.slice(0,10));
    }

    return (
        <button className="roller__dice" onClick={() => Roll(diceValue, opposed, setDisplay, history, setHistory)}>
            {diceValue}
        </button>
    );
}

function History({history}) {
    let historyItems = history.map((item, index) =>
        <div className="roller__historyLog" key={index}>
            <div className="roller__historyLogIndex">{index + 1 + ": "}</div>
            {item}
        </div>
    );

    return(
        <div className="roller__history">
            {historyItems}
        </div>
    );
}

export default Roller;