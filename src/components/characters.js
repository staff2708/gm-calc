import React, { useEffect, useState } from 'react';
import firebase from 'firebase/app';
import 'firebase/firestore';


function Characters() {
    useEffect(() => {
        firebase.firestore().collection('characters').get().then((snapshot) => {
            const tempArr = []
            snapshot.docs.forEach(doc => {
                tempArr.push(doc.data());
            })
            setList(tempArr);
            console.log(tempArr);
        })
    }, [])

    const [list, setList] = useState([]);

    return (
        <ul className="characters__list">
            {list.map((char) => {
                return (
            <li className="characters__item">
                <p>Имя: {char.name}</p>
                <p>Ближний бой: {char.melee}</p>
                <p>Дальний бой: {char.range}</p>
                <p>Блок: {char.block}</p>
                <p>Усиления тела с помощью шинсу: {char.shinsu_body_buff}</p>
                <p>Уворот:{char.dodge}</p>
                <p>Фехтование:{char.fencing}</p>
                <p>Физ подготовка:{char.physical_training}</p>
                <p>ХП:{char.hp}</p>
                <p>Урон:{char.damage}</p>
                <p>Сопротивление шинсу:{char.shinsu_resistance}</p>
                <p>Контроль шинсу:{char.shinsu_control}</p>
                <p>Количество домов шинсу:{char.baang}</p>
                <p>Размер домов шинсу:{char.myun}</p>
                <p>Плотность домов шинсу:{char.soo}</p>
                <p>Контроль светочей:{char.lighthouses}</p>
                <p>Fatigue:{char.fatigue}</p>
                <section>
                    <p>Skills:</p>
                    <ul>
                        {char.skills.map(skill => (
                            <li>
                                <div>{skill.name}</div>
                                <div>{skill.description}</div>
                                <div>{skill.fatigue}</div>
                            </li>
                        ))}
                    </ul>
                </section>
            </li>
                )
            })}
        </ul>
    );
}

export default Characters;