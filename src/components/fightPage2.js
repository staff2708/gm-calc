import React, { useEffect, useState } from 'react';
import firebase from 'firebase/app';
import './fightPage.css';

const Fight = () => {
    useEffect(() => {
        firebase.firestore().collection('characters').get().then((snapshot) => {
            const tempArr = []
            snapshot.docs.forEach(doc => {
                console.log(doc)
                let el = doc.data();
                el.fbid = doc.id;
                tempArr.push(el);
            })
            tempArr[0].weaponList = [
                {
                    name: 'None',
                    dmg: 0,
                    
                },
                {
                    name: 'Samehada',
                    dmg: 120,

                }, 
            ];
            tempArr[0].currentWeapon = {
                name: 'None',
                dmg: 0,
                
            };
            tempArr[1].weaponList = [
                {
                    name: 'None',
                    dmg: 0,
                    
                },
                {
                    name: 'Samehada',
                    dmg: 120,

                }, 
            ];
            tempArr[1].currentWeapon = {
                name: 'None',
                dmg: 0,
                
            };
            console.log('Char list: ', tempArr);
            setCharList(tempArr);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [charList, setCharList] = useState(null);
    const [firstChar, setFirstChar] = useState(null);
    const [secondChar, setSecondChar] = useState(null);
    const [calcResult, setCalcResult] = useState({
        firstCharRoll: null,
        secondCharRoll: null,
        dmgResult: null,
        resultComment: null,
    });

    const getD100 = () => {
        const roll = Math.floor(Math.random() * 100) + 1;
        console.log('roll d100: ', roll);
        return roll;
    }

    const calculateMelee = (dodgeChar) => {
        const firstCharRoll = getD100();
        const secondCharRoll = getD100();
        let dmgResult = 0;
        let resultComment = '';
        const isAutoSuccessByDiff = (firstChar.melee - secondChar[dodgeChar]) > 20;
        const isAutoFailByDiff = (secondChar[dodgeChar] - firstChar.melee) > 20;
        if(isAutoSuccessByDiff) {
            dmgResult = firstChar.physical_training + firstChar.shinsu_body_buff + firstChar.melee + firstChar.currentWeapon.dmg - secondChar.shinsu_body_buff;
            resultComment = 'Autosuccess by difference in characteristics more than 20';
        } else if(isAutoFailByDiff) {
            resultComment = 'Autofail by difference in characteristics more than 20';
        } else {
            if(firstCharRoll - firstChar.melee * 5 <= secondCharRoll - secondChar[dodgeChar] * 5) {
                dmgResult = (firstChar.physical_training + firstChar.shinsu_body_buff + firstChar.melee + firstChar.currentWeapon.dmg) * ((100-firstCharRoll)/100) - secondChar.shinsu_body_buff * ((100-secondCharRoll)/100);
                resultComment = 'Successfull melee attack';
            } else {
                resultComment = 'Failed melee attack';
            }
        }
        setCalcResult({ firstCharRoll, secondCharRoll, dmgResult, resultComment, })
    }

    const changeCurrentWeapon = (charFbid, newWeaponName) => {
        const charListClone = JSON.parse(JSON.stringify(charList));
        let selectedChar = charListClone.find(char => char.fbid === charFbid);
        let newWeaponObj = selectedChar.weaponList.find(weapon => weapon.name === newWeaponName);
        selectedChar.currentWeapon = newWeaponObj;
        //firebase.firestore().collection('characters').doc(charFbid).update(selectedChar);
        setCharList(charListClone);
    }

    // const calculateFencing = (DodgeChar) => {
    //     console.log(state);
    //     const isSuccess = (getD100() - state.firstCharacter.fencing * 5) <= (getD100() - state.secondCharacter[DodgeChar] * 5);
    //     if(isSuccess) {
    //         const dmg = state.firstCharacter.damage * 5 - state.secondCharacter.shinsu_body_buff * 5;
    //         if (dmg) {
    //             alert(`attacked, dmg: ${dmg}`);
    //         } else {
    //             alert('attacked, but no damage');
    //         }
    //     } else {
    //         alert('dodged');
    //     }
    // }

    // const calculateRange = (DodgeChar) => {
    //     console.log(state);
    //     const isSuccess = (getD100() - state.firstCharacter.range * 5) <= (getD100() - state.secondCharacter[DodgeChar] * 5);
    //     if(isSuccess) {
    //         const dmg = state.firstCharacter.damage * 5 - state.secondCharacter.shinsu_body_buff * 5;
    //         if (dmg) {
    //             alert(`attacked, dmg: ${dmg}`);
    //         } else {
    //             alert('attacked, but no damage');
    //         }
    //     } else {
    //         alert('dodged');
    //     }
    // }

    // const calculateShinsu = (DodgeChar) => {
    //     console.log(state);
    //     const isSuccess = (getD100() - state.firstCharacter.shinsu_control * 5) <= (getD100() - state.secondCharacter[DodgeChar] * 5);
    //     if(isSuccess) {
    //         const dmg = state.firstCharacter.soo * 5 - state.secondCharacter.shinsu_resistance * 3;
    //         if (dmg) {
    //             alert(`attacked, dmg: ${dmg}`);
    //         } else {
    //             alert('attacked, but no damage');
    //         }
    //     } else {
    //         alert('dodged');
    //     }
    // }

    return (
        <div className="fight-page">
            <ul className="selection-list selecton-list__first-character">
                {charList && charList.map(item => 
                    <li 
                        key={item.name}
                        onClick={() => setFirstChar(item)} 
                        className="selection-list__item"
                    >
                        {item.name}
                    </li>
                )}
            </ul>
            <div className="fight-page__control-section">
                {firstChar && secondChar &&
                    <div>
                        <section>
                            <div>
                                Left Char Weapon:
                            </div>
                            <select onChange={(e) => changeCurrentWeapon(firstChar.fbid, e.target.value)}>
                                {firstChar.weaponList.map(weapon => 
                                    <option key={weapon.name} value={weapon.name}>{weapon.name}</option>
                                )}
                            </select>
                        </section>
                        <section className="first-player">
                            <h3>Melee</h3>
                            <button onClick={() => calculateMelee('block')}>Melee vs Block</button>
                            <button onClick={() => calculateMelee('dodge')}>Melee vs Dodge</button>
                            <button onClick={() => calculateMelee('fencing')}>Melee vs Fencing</button>
                            <button onClick={() => calculateMelee('shinsu_control')}>Melee vs Shinsu</button>
                            <button onClick={() => calculateMelee('lighthouses')}>Melee vs Lighthouse</button>
                            {/* <h3>Fencing</h3>
                            <button onClick={() => calculateFencing('block')}>Fencing vs Block</button>
                            <button onClick={() => calculateFencing('dodge')}>Fencing vs Dodge</button>
                            <button onClick={() => calculateFencing('fencing')}>Fencing vs Fencing</button>
                            <button onClick={() => calculateFencing('shinsu_control')}>Fencing vs Shinsu</button>
                            <button onClick={() => calculateFencing('lighthouses')}>Fencing vs Lighthouse</button>
                            <h3>Range</h3>
                            <button onClick={() => calculateRange('block')}>Range vs Block</button>
                            <button onClick={() => calculateRange('dodge')}>Range vs Dodge</button>
                            <button onClick={() => calculateRange('fencing')}>Range vs Fencing</button>
                            <button onClick={() => calculateRange('shinsu_control')}>Range vs Shinsu</button>
                            <button onClick={() => calculateRange('lighthouses')}>Range vs Lighthouse</button>
                            <h3>Shinsu</h3>
                            <button onClick={() => calculateShinsu('block')}>Shinsu vs Block</button>
                            <button onClick={() => calculateShinsu('dodge')}>Shinsu vs Dodge</button>
                            <button onClick={() => calculateShinsu('fencing')}>Shinsu vs Fencing</button>
                            <button onClick={() => calculateShinsu('shinsu_control')}>Shinsu vs Shinsu</button>
                            <button onClick={() => calculateShinsu('lighthouses')}>Shinsu vs Lighthouse</button> */}
                        </section>
                        <section className="second-player"></section>
                    </div>
                }
                {firstChar && secondChar &&
                    <div>
                        {calcResult.firstCharRoll && <div>{firstChar.name} roll result: {calcResult.firstCharRoll}</div>}
                        {calcResult.secondCharRoll && <div>{secondChar.name} roll result: {calcResult.secondCharRoll}</div>}
                        {calcResult.resultComment && <div>Final result: {calcResult.resultComment}</div>}
                        {calcResult.dmgResult && <div>Damage: {calcResult.dmgResult}</div>}
                    </div>
                }
            </div>
            <ul className="selection-list selecton-list__second-character">
                {charList && charList.map(item => 
                    <li 
                        key={item.name}
                        onClick={() => setSecondChar(item)} 
                        className="selection-list__item"
                    >
                        {item.name}
                    </li>
                )}
            </ul>
        </div>
    )
}

export default Fight;