import React, { useState } from 'react';
import './calculator.css';

const defaultState = {
    attackAttribute: 0,
    attackAttributeDebuf: 0,
    damageAttribute: 0,
    dmgMultiplier: 1,
    bonusDmg: 0,
    bonusDmgMultiplier: 1, 
    defenceAttribute: 0,
    defenceAttributeDebuf: 0, 
    damageReductionAttribute: 0, 
    damageReductionMultiplier: 1, 
    bonusDefence: 0, 
    bonusDefenceMultiplier: 1
}

const Calculator = () => {

    const [state, setState] = useState(defaultState);

    const updateValue = (charName, value) => {
        setState({...state, [charName]: +value})
    }

    const calculateDmg = () => {
        const { 
            attackAttribute,
            damageAttribute,
            dmgMultiplier,
            bonusDmg,
            bonusDmgMultiplier, 
            defenceAttribute, 
            damageReductionAttribute, 
            damageReductionMultiplier, 
            bonusDefence, 
            bonusDefenceMultiplier
        } = state;
        const rollFirst = Math.floor(Math.random() * 100) + 1;
        const rollSecond = Math.floor(Math.random() * 100) + 1;
        const isSuccess = (rollFirst - attackAttribute * 5) <= (rollSecond - defenceAttribute * 5);
        console.log('roll first', rollFirst, 'rollSecond', rollSecond, state);
        if(isSuccess) {
            const dmg = (damageAttribute * dmgMultiplier + bonusDmg * bonusDmgMultiplier) - (damageReductionAttribute*damageReductionMultiplier + bonusDefence*bonusDefenceMultiplier);
            if (dmg) {
                alert(`attacked, dmg: ${dmg}`);
            } else {
                alert('attacked, but no damage');
            }
        } else {
            alert('dodged');
        }
    }

    return (
        <section className="calculator">
            <div className="calculator__character">
                <input type="number" onChange={e => updateValue('attackAttribute', e.target.value)} /> Attack attribute
                <input type="number" onChange={e => updateValue('attackAttributeDebuf', e.target.value)} /> Attack attribute debuf
                <input type="number" onChange={e => updateValue('damageAttribute', e.target.value)}/> Damage attribute
                <input type="number" onChange={e => updateValue('dmgMultiplier', e.target.value)}/> Multiplier for damage attribute
                <input type="number" onChange={e => updateValue('bonusDmg', e.target.value)}/> bonus damage
                <input type="number" onChange={e => updateValue('bonusDmgMultiplier', e.target.value)}/> Multiplier for bonus damage
            </div>
            <div className="calculator__character">
                <input type="number" onChange={e => updateValue('defenceAttribute', e.target.value)}/> Defanse attribute
                <input type="number" onChange={e => updateValue('defenceAttributeDebuf', e.target.value)} /> Defence attribute debuf
                <input type="number" onChange={e => updateValue('damageReductionAttribute', e.target.value)}/> Damage reduction attribute
                <input type="number" onChange={e => updateValue('damageReductionMultiplier', e.target.value)}/> multiplier for damage reduction attribute
                <input type="number" onChange={e => updateValue('bonusDefence', e.target.value)}/> bonus defence
                <input type="number" onChange={e => updateValue('bonusDefenceMultiplier', e.target.value)}/> Multiplier for bonus defence
            </div>
            <button onClick={calculateDmg}>fight</button>
        </section>
    )
}

export default Calculator;