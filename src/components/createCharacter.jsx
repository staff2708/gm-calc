import React, { useState } from 'react';
import  { v4 as uuidv4 } from 'uuid';
import firebase from 'firebase/app';
import './createCharacter.css';

const defaultState = {
    baang: 0,
    block: 0,
    damage: 0,
    dodge: 0,
    fatigue: 0,
    fencing: 0,
    hp: 0,
    lighthouses: 0,
    melee: 0,
    myun: 0,
    max_myun: 0,
    max_soo: 0,
    tension: 0,
    name: "",
    physical_training: 0,
    range: 0,
    shinsu_body_buff: 0,
    shinsu_control: 0,
    shinsu_resistance: 0,
    skills: [],
    weapon: [],
    shields: [],
    gold: 0,
    soo: 0,
}

const characteristicsMap = {
    baang: 'Количество домов шинсу',
    block: 'Блок',
    dodge: 'Уворот',
    fatigue: "Fatigue",
    fencing: 'Фехтование',
    lighthouses: 'Количество светочей',
    melee: 'Ближний бой',
    myun: 'размер домов',
    max_myun: 'максимальный размер дома шинсу',
    max_soo: 'максимальная плотность шинсу', 
    tension: 'напряжение шинсу',
    physical_training: 'физ подготовкаа',
    range: 'дальнийй бой',
    shinsu_body_buff: 'укрепление тела с помощью шинсу',
    shinsu_control: 'контроль шинсу',
    shinsu_resistance: 'сопротивление шинсу',
    soo: 'плотность шинсу',
};

const CreateCharacter  = () => {

    const [state, setState] = useState(defaultState);

    const updateValue = (charName, value) => {
        setState({...state, [charName]: +value})
    }

    const updateSkill = (skillId, skillAttribute, value) => {
        const updated = state.skills.map(item => {
            if(item.id === skillId) {
                return {...item, [skillAttribute]: value};
            }
            return item;
        })
        setState({...state, skills: updated})
    }


    const updateShield = (shieldId, shieldAttribute, value) => {
        const updated = state.shields.map(item => {
            if(item.id === shieldId) {
                return {...item, [shieldAttribute]: value};
            }
            return item;
        })
        setState({...state, shields: updated})
    }

    const updateWeapon = (weaponId, weaponAttribute, value) => {
        const updated = state.weapon.map(item => {
            if(item.id === weaponId) {
                return {...item, [weaponAttribute]: value};
            }
            return item;
        })
        setState({...state, weapon: updated})
    }

    const saveCharacter = () => {
        const data = {...state, hp: state.physical_training * 15, damage: state.physical_training * 3}
        firebase.firestore().collection('characters').add(data).then(() => {
            setState(defaultState);
        });
    }

    const addNewSkill = () => {
        setState({...state, skills: [...state.skills, {id: uuidv4(), name: '', description: '', fatigue: ''}]})
    }

    const addNewWeapon = () => {
        setState({...state, weapon: [...state.weapon, {id: uuidv4(), name: '', damage: ''}]})
    }


    const addNewShield = () => {
        setState({...state, shields: [...state.shields, {id: uuidv4(), name: '', block: ''}]})
    }

    const deleteSkill = (skillId) => {
        const { skills } = state;
        const newSkillsList = skills.filter(item => item.id !== skillId);
        setState({...state, skills: newSkillsList});
    }

    const deleteWeapon = (weaponId) => {
        const { weapon } = state;
        const newEquipmentList = weapon.filter(item => item.id !== weaponId);
        setState({...state, weapon: newEquipmentList});
    }

    const deleteShield = (shieldId) => {
        const { shields } = state;
        const newEquipmentList = shields.filter(item => item.id !== shieldId);
        setState({...state, shields: newEquipmentList});
    }

    return (
        <section className="newCharacter">
            <div className="newCharacter__characteristic characteristic">
                    <p className="characteristic__name">Имя</p>
                    <input type="text" className="characteristic__value" onChange={e => setState({...state, name: e.target.value})} value={state.name}/>
            </div>
            {Object.keys(characteristicsMap).map((characteristic) => {
                return (
                    <div className="newCharacter__characteristic characteristic" key={characteristic}>
                        <p className="characteristic__name">{characteristicsMap[characteristic]}</p>
                        <input type="text" className="characteristic__value" onChange={e => updateValue(characteristic, e.target.value)} value={state[characteristic]}/>
                    </div>
                )
            })}
            <div className="newCharacter__skills">
                {state.skills.map(skill => {
                    return (
                        <div className="newCharacter__skill skill" key={skill.id}>
                            Skill name: <input type="text" className="skill__name" value={skill.name} onChange={e => updateSkill(skill.id, 'name', e.target.value)}/>
                            Skill Description: <textarea className="skill__description" value={skill.description} onChange={e => updateSkill(skill.id, 'description', e.target.value)}/>
                            Skill Fatigue: <input type="text" className="skill__fatigue" value={skill.fatigue} onChange={e => updateSkill(skill.id, 'fatigue', e.target.value)}/>
                            <button onClick={() => deleteSkill(skill.id)}>delete skill</button>
                        </div>
                    )
                })}
                <button onClick={addNewSkill}>add new skill</button>
            </div>
            <div className="newCharacter__weapon">
            {state.weapon.map(weapon => {
                    return (
                        <div className="newCharacter__weapon weapon" key={weapon.id}>
                            weapon name: <input type="text" className="weapon__name" value={weapon.name} onChange={e => updateWeapon(weapon.id, 'name', e.target.value)}/>
                            weapon damage: <input type="text" className="weapon__damage" value={weapon.damage} onChange={e => updateWeapon(weapon.id, 'damage', e.target.value)}/>
                            <button onClick={() => deleteWeapon(weapon.id)}>delete weapon</button>
                        </div>
                    )
                })}
                <button onClick={addNewWeapon}>add new weapon</button>
            </div>
            <div>
            {state.shields.map(shield => {
                    return (
                        <div className="newCharacter__shield shield" key={shield.id}>
                            shield name: <input type="text" className="shield__name" value={shield.name} onChange={e => updateShield(shield.id, 'name', e.target.value)}/>
                            shield block: <input type="text" className="shield__block" value={shield.block} onChange={e => updateShield(shield.id, 'block', e.target.value)}/>
                            <button onClick={() => deleteShield(shield.id)}>delete shield</button>
                        </div>
                    )
                })}
                <button onClick={addNewShield}>add new shield</button>
            </div>
            <div className="newCharacter__gold">
                <p className="characteristic__name">Деньги</p>
                <input type="text" className="characteristic__value" onChange={e => updateValue('gold', e.target.value)} value={state['gold']}/>
            </div>
            <button onClick={saveCharacter}>save character</button>
        </section>
    )
}

export default CreateCharacter;